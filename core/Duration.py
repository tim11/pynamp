from dataclasses import dataclass


@dataclass(frozen=True)
class Duration:
    time: int = 0

    @property
    def hours(self) -> int:
        return self.time // 3600

    @property
    def minutes(self) -> int:
        hours = self.hours
        return (self.time - hours * 3600) // 60

    @property
    def seconds(self):
        hours = self.hours
        minutes = self.minutes
        return self.time - hours * 3600 - minutes * 60

    def __repr__(self):
        output = list()
        hours = self.hours
        if hours:
            output.append(str(hours).rjust(2, '0'))
        output.append(str(self.minutes).rjust(2, '0'))
        output.append(str(self.seconds).rjust(2, '0'))
        return ':'.join(output)
