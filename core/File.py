import pathlib
from os import path


class File:
    __allowed_extension: list[str] = ['mp3', 'wav', 'ogg']

    def __init__(self, src: str):
        if not path.exists(src) or not path.isfile(src):
            raise Exception(f'File {src} does not exists')
        extension = pathlib.Path(src).suffix[1:]
        if not self.is_allowed_extension(extension):
            raise Exception(f'Support only {", ".join(self.__allowed_extension)} types of file')
        self.__src = src
        self.__extension = extension
        self.__name = pathlib.Path(src).stem

    def is_allowed_extension(self, extension: str):
        return extension in self.__allowed_extension

    @property
    def src(self) -> str:
        return self.__src

    @property
    def extension(self) -> str:
        return self.__extension

    @property
    def is_mp3(self) -> bool:
        return self.extension == 'mp3'

    @property
    def is_wav(self) -> bool:
        return self.extension == 'wav'

    @property
    def is_ogg(self) -> bool:
        return self.extension == 'ogg'

    @property
    def name(self, with_extension: bool = False) -> str:
        name = self.__name
        if with_extension:
            name += '.' + self.extension
        return name

    @staticmethod
    def allowed_extension():
        return File.__allowed_extension
