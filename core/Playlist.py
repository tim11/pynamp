from .File import File


class Playlist:
    def __init__(self, files: list[File] = None):
        self.__files = files if files else list()
        self.__ix = 0

    def __has(self, file: File) -> bool:
        return any(map(lambda f: f.src == file.src, self))

    def __len__(self) -> int:
        return len(self.__files)

    def __iadd__(self, file: File):
        if not self.__has(file):
            self.__files.append(file)
        return self

    def __repr__(self):
        return f'Playlist({self.size} files)'

    def __getitem__(self, key: int):
        if key >= self.size or key < 0:
            raise KeyError
        return self.__files[key]

    def __iter__(self):
        self.__ix = 0
        return self

    def __next__(self):
        if self.__ix < self.size:
            result = self[self.__ix]
            self.__ix += 1
            return result
        raise StopIteration

    @property
    def size(self) -> int:
        return len(self)

    def clear(self):
        self.__files.clear()

    def add(self, file: File):
        self.__iadd__(file)

