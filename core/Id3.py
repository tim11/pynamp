from mutagen.id3 import ID3
from mutagen.mp3 import MP3
from mutagen.wave import WAVE
from mutagen.oggvorbis import OggVorbis
from .File import File
from .Duration import Duration


class Id3:
    @staticmethod
    def __get_info(reader: ID3 | None, param: str, default: str = None) -> str | None:
        if not reader:
            return default
        value = reader.get(param)
        return value.text[0] if value else default

    def __init__(self, file: File):
        reader = ID3(file.src) if file.is_mp3 else None
        self.__artist = self.__get_info(reader, 'TPE1')
        self.__title = self.__get_info(reader, 'TIT2', file.name)
        self.__year = self.__get_info(reader, 'TDRC')
        self.__album = self.__get_info(reader, 'TALB')
        if file.is_mp3:
            audio = MP3(file.src)
        elif file.is_wav:
            audio = WAVE(file.src)
        else:
            audio = OggVorbis(file.src)
        self.__duration = Duration(int(audio.info.length))

    @property
    def artist(self) -> str | None:
        return self.__artist

    @property
    def title(self) -> str:
        return self.__title

    @property
    def album(self) -> str | None:
        return self.__album

    @property
    def year(self) -> int | None:
        return int(self.__year) if self.__year else None

    @property
    def duration(self) -> Duration:
        return self.__duration
