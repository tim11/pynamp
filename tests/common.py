from os import path


def get_test_file(extension: str):
    file = path.abspath(
        path.join(
            './assets',
            f'file_example.{extension}'
        )
    )
    return file
