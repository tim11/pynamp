import unittest
from core import File
from common import get_test_file


class TestFile(unittest.TestCase):

    def test_success_mp3_file(self):
        filename = get_test_file('mp3')
        file = File(filename)
        self.assertEqual(file.src, filename)
        self.assertEqual(file.extension, 'mp3')
        self.assertTrue(file.is_mp3)
        self.assertEqual(file.name, 'file_example')

    def test_success_wav_file(self):
        filename = get_test_file('wav')
        file = File(filename)
        self.assertEqual(file.src, filename)
        self.assertEqual(file.extension, 'wav')
        self.assertTrue(file.is_wav)
        self.assertEqual(file.name, 'file_example')

    def test_success_ogg_file(self):
        filename = get_test_file('ogg')
        file = File(filename)
        self.assertEqual(file.src, filename)
        self.assertEqual(file.extension, 'ogg')
        self.assertTrue(file.is_ogg)
        self.assertEqual(file.name, 'file_example')

    def test_not_exists_file(self):
        filename = get_test_file('tmp')
        flag = True
        try:
            File(filename)
        except Exception:
            flag = False
        self.assertFalse(flag)

    def test_unsupported_file(self):
        filename = get_test_file('wma')
        flag = True
        try:
            File(filename)
        except Exception:
            flag = False
        self.assertFalse(flag)


if __name__ == '__main__':
    unittest.main()
