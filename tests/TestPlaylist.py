import unittest

from core import File, Playlist
from common import get_test_file


class TestPlaylist(unittest.TestCase):
    def __test_common(self, playlist: Playlist):
        self.assertEqual(len(playlist), 3)
        self.assertEqual(playlist.size, 3)
        for file in playlist:
            self.assertTrue(type(file) == File)
        playlist.clear()
        self.assertEqual(playlist.size, 0)

    def test_empty_init(self):
        playlist = Playlist()
        files = [File(get_test_file(ext)) for ext in File.allowed_extension()]
        for file in files:
            playlist += file
        self.__test_common(playlist)

    def test_full_init(self):
        playlist = Playlist([File(get_test_file(ext)) for ext in File.allowed_extension()])
        self.__test_common(playlist)


if __name__ == '__main__':
    unittest.main()
