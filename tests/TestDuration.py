import unittest
from core import Duration


class TestDuration(unittest.TestCase):
    def test_success(self):
        duration = Duration(time=3869)
        self.assertEqual(duration.hours, 1)
        self.assertEqual(duration.minutes, 4)
        self.assertEqual(duration.seconds, 29)
        self.assertEqual(str(duration), '01:04:29')


if __name__ == '__main__':
    unittest.main()
