import unittest
from common import get_test_file
from core import File, Id3


class TestId3(unittest.TestCase):

    def test_mp3_file(self):
        id3 = Id3(File(get_test_file('mp3')))
        self.assertEqual(id3.title, 'Impact Moderato')
        self.assertEqual(id3.artist, 'Kevin MacLeod')
        self.assertEqual(id3.album, 'YouTube Audio Library')
        self.assertIsNone(id3.year)
        self.assertEqual(id3.duration.hours, 0)
        self.assertEqual(id3.duration.minutes, 0)
        self.assertEqual(id3.duration.seconds, 27)

    def __test_other_extensions(self, file: File, id3: Id3):
        self.assertEqual(id3.title, file.name)
        self.assertIsNone(id3.artist)
        self.assertIsNone(id3.album)
        self.assertIsNone(id3.year)

    def test_ogg_file(self):
        file = File(get_test_file('ogg'))
        id3 = Id3(file)
        self.__test_other_extensions(file, id3)
        self.assertEqual(id3.duration.hours, 0)
        self.assertEqual(id3.duration.minutes, 1)
        self.assertEqual(id3.duration.seconds, 14)

    def test_wav_file(self):
        file = File(get_test_file('wav'))
        id3 = Id3(file)
        self.__test_other_extensions(file, id3)
        self.assertEqual(id3.duration.hours, 0)
        self.assertEqual(id3.duration.minutes, 0)
        self.assertEqual(id3.duration.seconds, 33)


if __name__ == '__main__':
    unittest.main()
